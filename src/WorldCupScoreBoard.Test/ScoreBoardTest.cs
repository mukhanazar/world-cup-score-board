using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WorldCupScoreBoard.Test
{
    [TestClass]
    public class ScoreBoardTest
    {
        private IScoreBoard scoreBoard;

        [TestInitialize]
        public void TestInitialize()
        {
            scoreBoard = new ScoreBoard();
        }

        [TestMethod]
        public void TestStartGame()
        {
            var result1 = scoreBoard.StartGame("Mexico", "Canada");
            Assert.IsTrue(result1);
            var result2 = scoreBoard.StartGame("Mexico", "Canada");
            Assert.IsFalse(result2);
        }

        [TestMethod]
        public void TestUpdateGame()
        {
            var result1 = scoreBoard.UpdateGame("Mexico", "Canada", 0, 5);
            Assert.IsFalse(result1);
            scoreBoard.StartGame("Mexico", "Canada");
            var result2 = scoreBoard.UpdateGame("Mexico", "Canada", 0, 5);
            Assert.IsTrue(result2);
        }

        [TestMethod]
        public void TestFinishGame()
        {
            var result1 = scoreBoard.FinishGame("Mexico", "Canada");
            Assert.IsFalse(result1);
            scoreBoard.StartGame("Mexico", "Canada");
            var result2 = scoreBoard.FinishGame("Mexico", "Canada");
            Assert.IsTrue(result2);
        }

        [TestMethod]
        public void TestGetSummary()
        {
            scoreBoard.StartGame("Mexico", "Canada");
            scoreBoard.StartGame("Spain", "Brazil");
            scoreBoard.StartGame("Germany", "France");
            scoreBoard.StartGame("Uruguay", "Italy");
            scoreBoard.StartGame("Argentina", "Australia");

            scoreBoard.StartGame("UAE", "Singapore");
            scoreBoard.UpdateGame("UAE", "Singapore", 10, 10);
            scoreBoard.FinishGame("UAE", "Singapore");

            scoreBoard.UpdateGame("Mexico", "Canada", 0, 5);
            scoreBoard.UpdateGame("Spain", "Brazil", 10, 2);
            scoreBoard.UpdateGame("Germany", "France", 2, 2);
            scoreBoard.UpdateGame("Uruguay", "Italy", 6, 6);
            scoreBoard.UpdateGame("Argentina", "Australia", 3, 1);

            var summary = scoreBoard.GetSummary();

            Assert.AreEqual("Uruguay 6 - Italy 6", summary[0]);
            Assert.AreEqual("Spain 10 - Brazil 2", summary[1]);
            Assert.AreEqual("Mexico 0 - Canada 5", summary[2]);
            Assert.AreEqual("Argentina 3 - Australia 1", summary[3]);
            Assert.AreEqual("Germany 2 - France 2", summary[4]);
        }
    }
}
