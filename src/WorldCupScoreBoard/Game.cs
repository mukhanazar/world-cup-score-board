﻿using System;

namespace WorldCupScoreBoard
{
    class Game
    {
        private string _homeTeam;

        private string _awayTeam;

        private int _homeScore;

        private int _awayScore;

        public Game(string homeTeam, string awayTeam)
        {
            _homeTeam = homeTeam;
            _awayTeam = awayTeam;
            Created = DateTime.UtcNow;
        }

        public int TotalScore
        {
            get
            {
                return _homeScore + _awayScore;
            }
        }

        public DateTime Created { get; private set; }

        public void UpdateScore(int homeScore, int awayScore)
        {
            _homeScore = homeScore;
            _awayScore = awayScore;
        }

        public override string ToString()
        {
            return $"{_homeTeam} {_homeScore} - {_awayTeam} {_awayScore}";
        }
    }
}
