﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WorldCupScoreBoard
{
    public class ScoreBoard : IScoreBoard
    {
        private Dictionary<(string, string), Game> games;

        public ScoreBoard()
        {
            games = new Dictionary<(string, string), Game>();
        }

        public bool StartGame(string homeTeam, string awayTeam)
        {
            var index = (homeTeam, awayTeam);
            if (!games.ContainsKey(index))
            {
                games.Add(index, new Game(homeTeam, awayTeam));
                return true;
            }

            return false;
        }

        public bool UpdateGame(string homeTeam, string awayTeam, int homeScore, int awayScore)
        {
            var index = (homeTeam, awayTeam);
            if (games.TryGetValue(index, out Game game))
            {
                game.UpdateScore(homeScore, awayScore);
                return true;
            }

            return false;
        }

        public bool FinishGame(string homeTeam, string awayTeam)
        {
            var index = (homeTeam, awayTeam);
            if (games.ContainsKey(index))
            {
                games.Remove(index);
                return true;
            }

            return false;
        }

        public string[] GetSummary()
        {
            return games
                .Select(kvp => kvp.Value)
                .OrderByDescending(game => game.TotalScore)
                .ThenByDescending(game => game.Created)
                .Select(game => game.ToString())
                .ToArray();
        }
    }
}
