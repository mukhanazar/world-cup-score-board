﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WorldCupScoreBoard
{
    public interface IScoreBoard
    {
        bool StartGame(string homeTeam, string awayTeam);

        bool UpdateGame(string homeTeam, string awayTeam, int homeScore, int awayScore);

        bool FinishGame(string homeTeam, string awayTeam);

        string[] GetSummary();
    }
}
